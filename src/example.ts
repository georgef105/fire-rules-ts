import { DatabaseRules, RulesOf } from "./rules.interface";

// Define your database schema
  
interface UserData {
  userName: string
  email: string
  signedUpAt: number
}
  
interface DatabaseSchema {
  users: Record<string, UserData>
}
  
// Use the DatabaseRules interface with your schema.

// NOTE - weird typescript issue with '.read' ect being expected to be of type of user. Type forced for now.
// TODO find a fix :/
const userRules: RulesOf<DatabaseSchema['users']> = {
  $uid: {
    userName: {
      '.read': 'auth != null',
      '.validate': 'newData.isString() && newData.val().length >= 2'
    },
    email: {
      '.validate': 'newData.isString()'
    },
    signedUpAt: {
      '.validate': 'newData.isNumber()'
    }
  },
  '.read': 'auth != null',
  '.write': 'auth.uid == $uid'
} as RulesOf<DatabaseSchema['users']>;

export const FIREBASE_RULES: DatabaseRules<DatabaseSchema> = {
  rules: {
    users: userRules
  }
};