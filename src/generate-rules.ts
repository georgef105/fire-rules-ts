import { DatabaseRules } from "./rules.interface";

export function generateRulesJson<T> (rules: DatabaseRules<T>): string {
  return JSON.stringify(rules, null, 2);
}