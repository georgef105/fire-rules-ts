#!/usr/bin/env node

import commander from 'commander';
import { writeFileSync } from 'fs';
import { generateRulesJson } from './generate-rules';
import { join } from 'path';

const DEFAULT_RULES_KEY = 'FIREBASE_RULES';

commander
  .option('-i, --input <string>', 'input file')
  .option('-o, --output <string>', 'output file')
  .option('-k, --key <string>', 'rules key');

commander.parse(process.argv);

const inputFilePath = commander.input;
const rulesKey = commander.key || DEFAULT_RULES_KEY;
const outputFilePath = commander.output;

const rules = require(join(process.cwd(), inputFilePath))[rulesKey];

writeFileSync(outputFilePath, generateRulesJson(rules));


