interface DatabaseRuleSet {
  '.read'?: string | boolean;
  '.write'?: string | boolean;
  '.validate'?: string | boolean;
  '.indexOn'?: string | string[];
}

// 16 levels since firebase allows max of 16 levels.
export type RulesOf<A> = {
  [B in keyof A]?: {
    [C in keyof A[B]]?: {
      [D in keyof A[B][C]]?: {
        [E in keyof A[B][C][D]]?: {
          [F in keyof A[B][C][D][E]]?: {
            [G in keyof A[B][C][D][E][F]]?: {
              [H in keyof A[B][C][D][E][F][G]]?: {
                [I in keyof A[B][C][D][E][F][G][H]]?: {
                  [J in keyof A[B][C][D][E][F][G][H][I]]?: {
                    [K in keyof A[B][C][D][E][F][G][H][I][J]]?: {
                      [L in keyof A[B][C][D][E][F][G][H][I][J][K]]?: {
                        [M in keyof A[B][C][D][E][F][G][H][I][J][K][L]]?: {
                          [N in keyof A[B][C][D][E][F][G][H][I][J][K][L][M]]?: {
                            [O in keyof A[B][C][D][E][F][G][H][I][J][K][L][M][N]]?: {
                              [P in keyof A[B][C][D][E][F][G][H][I][J][K][L][M][N][O]]?: DatabaseRuleSet
                            } & DatabaseRuleSet
                          } & DatabaseRuleSet
                        } & DatabaseRuleSet
                      } & DatabaseRuleSet
                    } & DatabaseRuleSet
                  } & DatabaseRuleSet
                } & DatabaseRuleSet
              } & DatabaseRuleSet
            } & DatabaseRuleSet
          } & DatabaseRuleSet
        } & DatabaseRuleSet
      } & DatabaseRuleSet
    } & DatabaseRuleSet
  } & DatabaseRuleSet
} & DatabaseRuleSet;

export interface DatabaseRules<Schema> {
  rules: RulesOf<Schema>
}
